﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace TCPSockets2
{
    class ThreadedEchoServer : IServer
    {
        // klasa obslugujaca polaczenie z jednym klientem
        class ClientHelper
        {
            Socket socket;    // otwarte gniazdo polaczenia
            NetworkStream ns; // strumien sieciowy "na gniezdzie"
            StreamReader sr;  // strumien do odbierania danych "na s.sieciowym"
            StreamWriter sw;  // strumien do wysylania danych "na s.sieciowym"
            ThreadedEchoServer server;
            public ClientHelper(Socket socket, ThreadedEchoServer server)
            {
                this.socket = socket;
                ns = new NetworkStream(this.socket);
                sr = new StreamReader(ns);
                sw = new StreamWriter(ns);
                sw.AutoFlush = true;
                this.server = server;
            }
  
            public void GetMessage()
        {
            string message;

            // czy ktores operacje tu wykonywane moga spowodowac wyjatek?
            sw.WriteLine("Client thread started.");
            //sw.Flush();
            do
            {
                // czekaj na komunikat
                message = sr.ReadLine();
                    if (message == null)
                    {
                      //  Disconnect();

                        break;
                    }
                Console.WriteLine(string.Format("Client @ {0} says: {1}", socket.RemoteEndPoint, message));

                // wyslij odpowiedz
                //sw.WriteLine(string.Format("OK. Got [{0}] Thanks!", message));
                //sw.Flush();
                server.SendMessage(message);

            }
            while (message != "QUIT!");
                Console.WriteLine(string.Format("Client @ {0} has been disconnected!", socket.RemoteEndPoint));
                Disconnect();
        }

            public void SendMessage(string message)
            {
                sw.WriteLine(string.Format("OK. Got [{0}] Thanks!", message));
            }


            void Disconnect()
            {
                // to nie sprawdza czy strumien jest juz Disposed...
                if (sw != null) sw.Close();
                if (sr != null) sr.Close();
                if (ns != null) ns.Close();
                if (socket != null) socket.Close();
                server.RemoveClient(this);
            }
        }


        IPEndPoint ipEndPoint;
        Socket listeningSocket;
        List<ClientHelper> activeClients;

        public ThreadedEchoServer(IPAddress ipAddress, int ipPort)
        {
            ipEndPoint = new IPEndPoint(ipAddress, ipPort);
            listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            activeClients = new List<ClientHelper>();
            Console.WriteLine("Server created.");
        }

        public void Start()
        {
            listeningSocket.Bind(ipEndPoint);
            listeningSocket.Listen(1);
           


                Console.WriteLine("Server @ {0} started.", ipEndPoint);
                bool mark = true;
                do
                {
                    Console.WriteLine("Server @ {0} waits for a client...", ipEndPoint);

                Socket clientSocket;
                
               try
                {
                    clientSocket = listeningSocket.Accept(); // czy to moze zglosic wyjatek? - Tak, gdy wyłączymy serwer (skrót klawiszowy)

                    Console.WriteLine("Server @ {0} client connected @ {1}.", ipEndPoint, clientSocket.RemoteEndPoint);
                    Console.WriteLine("Server @ {0} starting client thread.", ipEndPoint, clientSocket.RemoteEndPoint);

                    // stworz obiekt obslugujacy polaczenie z klientem
                    ClientHelper ch = new ClientHelper(clientSocket, this);
                    activeClients.Add(ch);
                    // stworz nowy watek, przekaz w ctorze metode, ktora ma byc uruchomiona
                    Thread t = new Thread(ch.GetMessage);
                    t.Start();
                }
                catch (Exception e)
                {
                  //nic sie nie dzieje
                }
                           
                // uruchom watek (przekazana w ctorze metode)
                // ... i... zapomnij o tym (?)
                if(activeClients.Count==0)
                {
                    mark = false;
                }

                }
                while (mark);
        
        }

        public void SendMessage(string msg)
        {
            foreach (ClientHelper ch in activeClients)
            {
                ch.SendMessage(msg);
            }
        }
            
        void RemoveClient(ClientHelper ch)
        {
            activeClients.Remove(ch);
        }
        public void Stop()
        {

            listeningSocket.Close();
            // a co z ew. klientami?
            SendMessage("The server has been stopped"); //wysyła do wszystkich wiadomość
            activeClients.Clear();  // czyści listę klientów
            
        }
    } // class
} // namespace
