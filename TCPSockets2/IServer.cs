﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace TCPSockets2
{
    public interface IServer
    {
        void Start();
        void Stop();
    }
}
